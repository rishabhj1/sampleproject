package com.google.musicsearch.broadcastReceivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.google.musicsearch.event.Event;
import com.google.musicsearch.event.EventHandler;

public class NetworkConnectivityHandler extends BroadcastReceiver {

    private Event<Void> event = new Event<>();

    public NetworkConnectivityHandler(){

    }

    public void attachListenersForNetworkConnectivity(EventHandler<Void> eventHandler) {
        event.addEventHandler(eventHandler);
    }

    public void removeListeners(EventHandler<Void> eventHandler) {
        event.removeEventHandler(eventHandler);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager
                .getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnected()) {
            event.raiseEvent(null);
        }
    }
}
