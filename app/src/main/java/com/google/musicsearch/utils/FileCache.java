package com.google.musicsearch.utils;

import android.content.Context;

import java.io.File;

class FileCache {

    private File cacheDir;

    FileCache(Context context) {
        cacheDir = new File(context.getFilesDir(), "SongsTemp");
        if (!cacheDir.exists()) {
            cacheDir.mkdirs();
        }
    }

    File getFile(String url) {
        String filename = String.valueOf(url.hashCode());
        return new File(cacheDir, filename);
    }
}
