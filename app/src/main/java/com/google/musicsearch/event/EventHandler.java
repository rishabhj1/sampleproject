package com.google.musicsearch.event;

public interface EventHandler<T> {
    void onTaskCompleted(T result);
}
