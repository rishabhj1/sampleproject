package com.google.musicsearch.event;

import java.util.ArrayList;
import java.util.List;

public class Event<T> {

    private List<EventHandler<T>> eventHandlers = new ArrayList<>();

    public void addEventHandler(EventHandler<T> eventHandler) {
        eventHandlers.add(eventHandler);
    }

    public void removeEventHandler(EventHandler<T> eventHandler) {
        eventHandlers.remove(eventHandler);
    }

    public void raiseEvent(T result) {
        for (EventHandler eventHandler : eventHandlers) {
            eventHandler.onTaskCompleted(result);
        }
    }
}
