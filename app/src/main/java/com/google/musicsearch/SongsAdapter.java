package com.google.musicsearch;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.musicsearch.models.Song;
import com.google.musicsearch.utils.ImageLoader;

import java.util.List;

public class SongsAdapter extends RecyclerView.Adapter<SongsAdapter.ViewHolder> {

    private List<Song> songsList;
    private ImageLoader imageLoader;

    public SongsAdapter(List<Song> songs, Context context) {
        songsList = songs;
        imageLoader = new ImageLoader(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_layout, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.nameView.setText(songsList.get(position).trackName);
        holder.priceView.setText(String.valueOf(songsList.get(position).trackPrice));
        imageLoader.displayImage(songsList.get(position).artworkUrl100, holder.imageView);
        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                view.getContext().startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(songsList.get(position).trackViewUrl)));
            }
        });
    }

    public void setSongsList(List<Song> songsList) {
        this.songsList = songsList;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return songsList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        View view;
        ImageView imageView;
        TextView nameView;
        TextView priceView;

        public ViewHolder(View view) {
            super(view);
            this.view = view;
            imageView = view.findViewById(R.id.image_view);
            nameView = view.findViewById(R.id.name_view);
            priceView = view.findViewById(R.id.price_view);
        }
    }
}
