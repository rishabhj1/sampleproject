package com.google.musicsearch.models;

public class Song {
    public String trackName;
    public float trackPrice;
    public String artworkUrl100;
    public String trackViewUrl;
}