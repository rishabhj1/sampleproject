package com.google.musicsearch;

import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.google.musicsearch.broadcastReceivers.NetworkConnectivityHandler;
import com.google.musicsearch.event.EventHandler;
import com.google.musicsearch.models.Song;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static android.net.ConnectivityManager.CONNECTIVITY_ACTION;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.toString();
    private static final String KEY_SONGS = "songs";
    public static final String SONGS_LIST_URL = "https://itunes.apple.com/search?term=Michael+jackson";
    private SongsAdapter songsListAdapter;
    private KVStore kvStore;
    private final NetworkConnectivityHandler networkConnectivityHandler = new NetworkConnectivityHandler();
    private final EventHandler<Void> networkConnectivityEventHandler = new EventHandler<Void>() {
        @Override
        public void onTaskCompleted(Void result) {
            tryToFetchSongsList();
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        registerNetworkConnectivityListener();
        songsListAdapter = new SongsAdapter(new ArrayList<Song>(0), this);
        kvStore = new KVStore(this);
        initViews();
        downloadSongsData();
        networkConnectivityHandler.attachListenersForNetworkConnectivity(networkConnectivityEventHandler);
    }

    private void registerNetworkConnectivityListener() {
        IntentFilter intentFilter = new IntentFilter(CONNECTIVITY_ACTION);
        registerReceiver(networkConnectivityHandler, intentFilter);
    }

    private void tryToFetchSongsList() {
        if (kvStore.getString(KEY_SONGS) != null)
            return;
        downloadSongsData();
    }

    private void initViews() {
        setContentView(R.layout.activity_main);
        RecyclerView songsView = findViewById(R.id.songs_list);
        songsView.setAdapter(songsListAdapter);
        songsView.setLayoutManager(new LinearLayoutManager(this));
    }

    private void downloadSongsData() {
        NetworkCall networkCall = new NetworkCall();
        networkCall.addListenerOnTaskCompletion(new EventHandler<String>() {
            @Override
            public void onTaskCompleted(String result) {
                Log.d(TAG, "Result is " + result);
                if (result == null && kvStore.getString(KEY_SONGS) != null) {
                    Log.i(TAG, "Using cached songs data");
                    result = kvStore.getString(KEY_SONGS);
                }
                try {
                    List<Song> songs = parseSongsJson(result);
                    songsListAdapter.setSongsList(songs);
                    kvStore.putString(KEY_SONGS, result);
                } catch (Exception e) {
                    Log.e(TAG, "Unable to parse songs list : " + e.getMessage());
                }
            }
        });
        networkCall.execute(SONGS_LIST_URL);
    }

    @NonNull
    private List<Song> parseSongsJson(String result) throws JSONException {
        JSONObject o = new JSONObject(result);
        JSONArray results = o.getJSONArray("results");
        List<Song> songs = new ArrayList<>();
        for (int i = 0; i < results.length(); ++i) {
            JSONObject songObj = results.getJSONObject(i);
            Song song = new Song();
            song.trackName = songObj.getString("trackName");
            song.trackPrice = Float.parseFloat(songObj.getString("trackPrice"));
            song.trackName = songObj.getString("trackName");
            song.trackViewUrl = songObj.getString("trackViewUrl");
            song.artworkUrl100 = songObj.getString("artworkUrl100");
            songs.add(song);
        }
        return songs;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        networkConnectivityHandler.removeListeners(networkConnectivityEventHandler);
    }
}
