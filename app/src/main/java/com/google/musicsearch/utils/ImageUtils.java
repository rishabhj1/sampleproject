package com.google.musicsearch.utils;

import android.graphics.BitmapFactory;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;

import static android.content.ContentValues.TAG;

public class ImageUtils {

    public static int scalingFactorForImage(BitmapFactory.Options o) {
        final int REQUIRED_SIZE = 85;
        int width_tmp = o.outWidth, height_tmp = o.outHeight;
        int scale = 1;
        while (true) {
            if (width_tmp / 2 < REQUIRED_SIZE || height_tmp / 2 < REQUIRED_SIZE)
                break;
            width_tmp /= 2;
            height_tmp /= 2;
            scale *= 2;
        }
        return scale;
    }

    public static void storeImageInFile(File f, HttpURLConnection conn) throws IOException {
        InputStream is = conn.getInputStream();
        OutputStream os = new FileOutputStream(f);
        copyStream(is, os);
        os.close();
        conn.disconnect();
    }

    private static void copyStream(InputStream is, OutputStream os) {
        final int buffer_size = 1024;
        try {
            byte[] bytes = new byte[buffer_size];
            for (; ; ) {
                int count = is.read(bytes, 0, buffer_size);
                if (count == -1)
                    break;
                os.write(bytes, 0, count);
            }
        } catch (Exception ex) {
            Log.e(TAG, "Error while copying stream : " + ex.getMessage());
        }
    }
}
