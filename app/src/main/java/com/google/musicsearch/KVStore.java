package com.google.musicsearch;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

public class KVStore {

    private SharedPreferences store;

    public KVStore(Activity activity)
    {
        store = activity.getPreferences(Context.MODE_PRIVATE);
    }

    public void putString(String key, String value){
        SharedPreferences.Editor edit = store.edit();
        edit.putString(key,value);
        edit.commit();
    }

    public String getString(String key){
        return store.getString(key, null);
    }
}
